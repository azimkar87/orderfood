﻿using System;
using System.Threading.Tasks;
using OrderFood.Core;
using OrderFood.Core.Models;
using OrderFood.Core.Repositories;

namespace OrderFood.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IRestaurantRepository Restaurants { get; }
        IFoodRepository Food { get; }
        IOrderRepository Order { get; }
        IBasketRepository Basket { get; }
        ICommentRepository Comment { get; }
        IRatingRepository Rating { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
