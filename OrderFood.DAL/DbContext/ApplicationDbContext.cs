﻿using OrderFood.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OrderFood.Models;

namespace OrderFood.DAL.DbContext
{
    public class ApplicationDbContext : IdentityDbContext<User,Role,int>
    {
        public DbSet<Food> Food { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Order> Order { get;  set; }
        public DbSet<Basket> Basket { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Rating> Rating { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
