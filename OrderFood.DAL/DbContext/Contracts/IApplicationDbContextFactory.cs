﻿namespace OrderFood.DAL.DbContext.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}