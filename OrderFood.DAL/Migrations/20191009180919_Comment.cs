﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OrderFood.DAL.Migrations
{
    public partial class Comment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CommentId",
                table: "Restaurants",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Restaurants_CommentId",
                table: "Restaurants",
                column: "CommentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Restaurants_Comment_CommentId",
                table: "Restaurants",
                column: "CommentId",
                principalTable: "Comment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Restaurants_Comment_CommentId",
                table: "Restaurants");

            migrationBuilder.DropIndex(
                name: "IX_Restaurants_CommentId",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "CommentId",
                table: "Restaurants");
        }
    }
}
