﻿using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderFood.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Order;
        }

        public override IEnumerable<Order> GetAll()
        {
            return DbSet.Include(p => p.Restaurant).Include(p=>p.Restaurant.Food).ToList();
        }

        public override Order GetById(int restaurantId)
        {
            return DbSet.Include(p => p.Restaurant).FirstOrDefault(p => p.Id == restaurantId);
        }

        public  IEnumerable<Order> Add(int foodId, int restaurantId)
        {
            return DbSet.Include(p => p.Restaurant).Where(p => p.RestaurantId == foodId && p.FoodId == restaurantId);
        }
    }
}
