﻿using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace OrderFood.DAL.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Restaurants;
        }

        public override Restaurant GetById(int id)
        {
            return DbSet.Include(p => p.Food).Include(p=>p.Comments).Include(p=>p.Ratings).FirstOrDefault(p => p.Id == id);
        }

        public Restaurant GetByName(string name)
        {
            return DbSet.FirstOrDefault(p => p.Name == name);
        }
    }
}
