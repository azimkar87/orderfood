﻿using Microsoft.EntityFrameworkCore;
using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderFood.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Comment;
        }

        public override IEnumerable<Comment> GetAll()
        {
            return DbSet.ToList();
        }

        public override Comment GetById(int id)
        {
            return DbSet.FirstOrDefault(p => p.Id == id);
        }
    }
}
