﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace OrderFood.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            DbSet = _context.Set<T>();
        }

        public async Task<T> AddAsync(T entity)
        {
            var entry = await DbSet.AddAsync(entity);

            return entry.Entity;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }
        //identity
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public virtual T GetById(int id)
        {
            return Queryable.FirstOrDefault(DbSet, e => e.Id == id);
        }

        public virtual IEnumerable<T> GetListById(int id)
        {
            return DbSet.Where(x => x.Id==id).ToList();
        }

        public void Update(T entity)
        {
            DbSet.Update(entity);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
    }
}
