﻿using OrderFood.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using OrderFood.Core.Models;
using OrderFood.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace OrderFood.DAL.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Rating;
        }

        public override IEnumerable<Rating> GetAll()
        {
            return DbSet.Include(p => p.restaurant).ToList();
        }

        public override Rating GetById(int id)
        {
            return DbSet.Include(p => p.restaurant).FirstOrDefault(p => p.Id == id);
        }
    }
}
