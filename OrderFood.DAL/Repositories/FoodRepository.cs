﻿using System.Collections.Generic;
using System.Linq;
using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace OrderFood.DAL.Repositories
{
    public class FoodRepository : Repository<Food>, IFoodRepository
    {
        public FoodRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Food;
        }

        public override IEnumerable<Food> GetAll()
        {
            return DbSet.Include(p => p.Restaurant).ToList();
        }

        public override Food GetById(int id)
        {
            return DbSet.Include(p => p.Restaurant).FirstOrDefault(p => p.Id == id);
        }

        public Food GetByName(string name)
        {
            return DbSet.FirstOrDefault(p => p.Name == name); 
        }

        public bool IsExistByName(string name)
        {
            return DbSet.Any(p => p.Name == name);
        }
    }
}
