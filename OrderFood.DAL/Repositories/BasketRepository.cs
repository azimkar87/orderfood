﻿using Microsoft.EntityFrameworkCore;
using OrderFood.Core.Models;
using OrderFood.Core.Repositories;
using OrderFood.DAL.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderFood.DAL.Repositories
{
    public class BasketRepository : Repository<Basket>, IBasketRepository
    {
        public BasketRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Basket;
        }

        public override IEnumerable<Basket> GetAll()
        {
            return DbSet.Include(p => p.restaurant).ToList();
        }

        public override Basket GetById(int id)
        {
            return DbSet.Include(p => p.restaurant).FirstOrDefault(p => p.Id == id);
        }

     

    }
}
