﻿using OrderFood.Core.Models;

namespace OrderFood.Core.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
        Restaurant GetByName(string foodName);
    }
}