﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OrderFood.Core.Models;

namespace OrderFood.Core.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> AddAsync(T entity);
        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync();
        T GetById(int id);
        IEnumerable<T> GetListById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
