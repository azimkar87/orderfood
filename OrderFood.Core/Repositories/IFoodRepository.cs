﻿using OrderFood.Core.Models;

namespace OrderFood.Core.Repositories
{
    public interface IFoodRepository : IRepository<Food>
    {
        Food GetByName(string foodName);
    }
}
