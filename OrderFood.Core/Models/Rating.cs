﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderFood.Core.Models
{
  public  class Rating:Entity
    {
        public int UserId { get; set; }
        public int RestaurantId { get; set; }
        public int Mark { get; set; }

  

        public Restaurant restaurant { get; set; }
    }
}
