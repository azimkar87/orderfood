﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderFood.Core.Models
{
   public class Order:Entity
    {
        public int UserId { get; set; }
        public int RestaurantId { get; set; }
        public int FoodId { get; set; }
        public int BasketId { get; set; }
        public bool isClosed { get; set; }

        public int Count { get; set; }

        public Restaurant Restaurant { get; set; }
    }
}
