﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OrderFood.Core.Models
{
    public class Comment : Entity
    {
        public int UserId { get; set; }
        public int RestaurantId { get; set; }
        public string Text { get; set; }

       
      //  public Restaurant restaurant { get; set; }
        
    }
}
