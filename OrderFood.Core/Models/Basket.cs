﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderFood.Core.Models
{
    public class Basket:Entity
    {
        public int UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public int RestaurantId { get; set; }
        public int OrderSum { get; set; }

        public Restaurant restaurant { get; set; }
    }
}
