﻿using System.Collections.Generic;


namespace OrderFood.Core.Models
{
    public class Restaurant : Entity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }

        public ICollection<Food> Food { get; set; }

        public ICollection<Order> Orders { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public ICollection<Rating> Ratings { get; set; }

        public Comment Comment { get; set; }

      

        public Restaurant()
        {
            Food = new List<Food>();
            Orders = new List<Order>();
            Comments = new List<Comment>();
            Ratings = new List<Rating>();
        }
    }
}
