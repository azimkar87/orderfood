﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace OrderFood.Core.Models
{
    public class Food : Entity
    {
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        public int Price { get; set; }
        public int RestaurantId { get; set; }
        [Display(Name = "Ресторан")]
      //  [JsonIgnore]
        public Restaurant Restaurant { get; set; }
    }
}
