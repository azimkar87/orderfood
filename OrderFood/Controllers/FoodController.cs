﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OrderFood;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.DAL.DbContext;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using OrderFood.ViewModels;
using OrderFood.DAL.DbContext;

namespace OrderFood.Controllers
{
    public class FoodController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFoodService _foodService;

        public FoodController(
            ApplicationDbContext context,
            IUnitOfWorkFactory unitOfWorkFactory,
            IFoodService foodService)
        {
            if(context == null)
                throw new ArgumentNullException(nameof(context));
            if(unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if(foodService == null)
                throw new ArgumentNullException(nameof(foodService));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
            _foodService = foodService;
        }

        // GET: Foods
        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var foods = unitOfWork.Food.GetAll();
                return View(foods);
            }
        }

        // GET: foods/Details
        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var food = unitOfWork.Food.GetById(id.Value);
                if (food == null)
                {
                    return NotFound();
                }

                return View(food);
            }
        }


        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var model = new FoodCreateModel
                {
                    RestaurantsSelectList = new SelectList(unitOfWork.Restaurants.GetAll(), "Id", "Name", "Price")
                };

                return View(model);
            }
        }

        // POST: /Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FoodCreateModel model)
        {
            var dto = Mapper.Map<FoodCreateDto>(model);

            var result = await _foodService.CreateFoodAsync(dto);

            if (result.IsSuccess)
            {
                return RedirectToAction("Index");
            }
            else
            {
                foreach (var resultError in result.Errors)
                {
                    ModelState.AddModelError("Error", resultError);
                }
                model.RestaurantsSelectList = new SelectList(_context.Restaurants, "Id", "Name", model.RestaurantId);
                return View(model);
            }
        }

        private void ValidatePosData(IUnitOfWork unitOfWork, string name)
        {
            var pos = unitOfWork.Food.GetByName(name);

            if(pos != null)
                ModelState.AddModelError("Error", $"Пункт с названием {name} уже существует");
        }

        // GET: Food/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var food = unitOfWork.Food.GetById(id.Value);

                var model = Mapper.Map<FoodEditModel>(food);

                model.RestaurantsSelectList = new SelectList(unitOfWork.Food.GetAll(), "Id", "Name", model.RestaurantId);
                
                return View(model);
            }
        }

        // POST: Foods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(FoodEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                ValidatePosData(unitOfWork, model.Name);
                if (ModelState.IsValid)
                {
                    var food = Mapper.Map<Food>(model);

                    unitOfWork.Food.Update(food);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }
                model.RestaurantsSelectList = new SelectList(unitOfWork.Restaurants.GetAll(), "Id", "Name", model.RestaurantId);
                return View(model);
            }
        }

        // GET: Foods/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var food = await _context.Food
                .Include(p => p.Restaurant)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (food == null)
            {
                return NotFound();
            }

            return View(food);
        }

        // POST: Foods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var food = await _context.Food.FindAsync(id);
            _context.Food.Remove(food);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FoodExists(int id)
        {
            return _context.Food.Any(e => e.Id == id);
        }
    }
}
