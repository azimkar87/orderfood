﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.DAL.DbContext;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace OrderFood.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RatingController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IRatingService _ratingService;

        public RatingController(
                   ApplicationDbContext context,
                   IUnitOfWorkFactory unitOfWorkFactory,
                   IRatingService ratingService)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
           if (ratingService == null)
               throw new ArgumentNullException(nameof(ratingService));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
            _ratingService = ratingService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? restaurantId, int? userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var orders = unitOfWork.Order.GetAll().Where(x=>x.RestaurantId==restaurantId.Value && x.UserId==userId.Value && x.isClosed!=true);
                return View(orders);
            }
        }

        [HttpPost]
        public async Task<IActionResult> setMark([FromForm] Rating rating )
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (rating.UserId==0)
                {
                    return NotFound();
                }
                if (rating.RestaurantId == 0)
                {
                    return NotFound();
                }
                var ratingInDb=unitOfWork.Rating.GetAll().Where(x => x.RestaurantId == rating.RestaurantId).FirstOrDefault();
                if (ratingInDb!= null) {
                ratingInDb.Mark = rating.Mark;
                unitOfWork.Rating.Update(ratingInDb);
                await unitOfWork.CompleteAsync();
                }
                else
                {
                    var result = await _ratingService.CreateRatingAsync(rating);
                }
                return RedirectToAction("Details", "Restaurant", new { id = rating.RestaurantId});
            }
        }
    }
}