﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.DAL.DbContext;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace OrderFood.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CommentController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly ICommentService _commentService;

        public CommentController(
                   ApplicationDbContext context,
                   IUnitOfWorkFactory unitOfWorkFactory,
                   ICommentService commentService)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
           if (commentService == null)
               throw new ArgumentNullException(nameof(commentService));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
            _commentService = commentService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? restaurantId, int? userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var orders = unitOfWork.Order.GetAll().Where(x=>x.RestaurantId==restaurantId.Value && x.UserId==userId.Value && x.isClosed!=true);
                return View(orders);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromForm] Comment comment )
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (comment.UserId==0)
                {
                    return NotFound();
                }
                if (comment.RestaurantId == 0)
                {
                    return NotFound();
                }

                var result = await _commentService.CreateCommentAsync(comment);
                return RedirectToAction("Index", "Restaurant", new { id = comment.RestaurantId});
            }
        }
    }
}