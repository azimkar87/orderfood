﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.DAL.DbContext;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderFood.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BasketController:Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IBasketService _basketService;

        public BasketController(
           ApplicationDbContext context,
           IUnitOfWorkFactory unitOfWorkFactory,
           IBasketService basketService)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (basketService == null)
                throw new ArgumentNullException(nameof(basketService));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
            _basketService = basketService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? userId)
        {

            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (userId.Value!=0)
                { 
                var orders = unitOfWork.Basket.GetAll().Where(x=>x.UserId==userId.Value);
                    return View(orders);
                }
                else
                {
                    var orders = unitOfWork.Basket.GetAll();
                    return View(orders);
                }
                
            }
        }





        // POST: PointOfSales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpGet]
        public async Task<IActionResult> Create(int? restaurantId,int? userId, int? sum )
        {
            Basket basket = new Basket();
            basket.OrderDate = DateTime.Now;
            basket.OrderSum = sum.Value;
            basket.RestaurantId = restaurantId.Value;
            basket.UserId = userId.Value;
            var dto = Mapper.Map<BasketCreateDto>(basket);

            var result = await _basketService.CreateBasketAsync(dto);

            if (result.IsSuccess)
            {
                return RedirectToAction("Index", new { userId = userId.Value });
            }
            else
            {
                foreach (var resultError in result.Errors)
                {
                    ModelState.AddModelError("Error", resultError);
                }
                return View("Error");
            }
        }

        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var orderList = unitOfWork.Order.GetAll().Where(x => x.BasketId == id.Value);
                return View(orderList);
            }
        }
    }
}
