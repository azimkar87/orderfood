﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.DAL.DbContext;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace OrderFood.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IOrderService _orderService;

        public OrderController(
                   ApplicationDbContext context,
                   IUnitOfWorkFactory unitOfWorkFactory,
                   IOrderService orderService)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
           if (orderService == null)
               throw new ArgumentNullException(nameof(orderService));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? restaurantId, int? userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var orders = unitOfWork.Order.GetAll().Where(x=>x.RestaurantId==restaurantId.Value && x.UserId==userId.Value && x.isClosed!=true);
                return View(orders);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Create(int? id, int? restaurantId, int? userId )
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }
                if (!restaurantId.HasValue)
                {
                    return NotFound();
                }

                var order = unitOfWork.Order.GetAll().Where(x=>x.FoodId==id && x.RestaurantId==restaurantId && x.UserId==userId && x.isClosed==false).FirstOrDefault();
                if (order == null)
                {
                    order = new Order();
                    order.Count = 1;
                    order.RestaurantId = restaurantId.Value;
                    order.FoodId = id.Value;
                    order.UserId = userId.Value;
                    var result = await _orderService.AddOrderAsync(order);
                    return RedirectToAction("Index", new { restaurantId = restaurantId.Value,userId=userId.Value });
                }
                else
                {
                    order.Count = order.Count + 1;
                    unitOfWork.Order.Update(order);
                    await unitOfWork.CompleteAsync();
                    return RedirectToAction("Index", new { restaurantId = restaurantId.Value, userId=userId.Value });
                }
         

            }
        }
    }
}