﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderFood.DAL;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

using OrderFood.DAL.DbContext;
using OrderFood.Domain.Services;
using OrderFood.Core.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace OrderFood.Controllers
{
    [Authorize(Roles="Admin,Client")]
    public class RestaurantController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IRestaurantService _restaurantService;
        private readonly ICommentService _commentService;
        IHostingEnvironment _appEnvironment;

        public RestaurantController(ApplicationDbContext context, 
                                    IUnitOfWorkFactory unitOfWorkFactory, 
                                    IHostingEnvironment appEnvironment,
                                    IRestaurantService restaurantService,
                                    ICommentService commentService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _appEnvironment = appEnvironment;
            _context = context;
            _restaurantService = restaurantService;
            _commentService = commentService;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var restaurants = unitOfWork.Restaurants.GetAll().Where(x => x.Name != null);
               
                return View(restaurants);
            }
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var model = new Restaurant
                {
                    Name = "",
                    Description = ""
                };
                
                return View(model);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Restaurant restaurant, IFormFile uploadedFile)
        {
          if (uploadedFile != null)
                    {
                        // путь к папке Files
                        string path = "/images/" + uploadedFile.FileName;
                        // сохраняем файл в папку Files в каталоге wwwroot
                        using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        restaurant.Image =uploadedFile.FileName;
                    }
            var result = await _restaurantService.CreateFoodAsync(restaurant);

            if (result.IsSuccess)
            {
                return RedirectToAction("Index");
            }
            else
            {
                foreach (var resultError in result.Errors)
                {
                    ModelState.AddModelError("Error", resultError);
                }
             
                return View(restaurant);
            }
        }

        // GET: 
        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var restaurant = unitOfWork.Restaurants.GetById(id.Value);
                if (restaurant == null)
                {
                    return NotFound();
                }
                
                return View(restaurant);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var restaurant = unitOfWork.Restaurants.GetById(id.Value);
                return View(restaurant);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Restaurant restaurant,IFormFile uploadedFile)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                ValidatePosData(unitOfWork, restaurant.Name);
                if (ModelState.IsValid)
                {
                    if (uploadedFile != null)
                    {
                        // путь к папке Files
                        string path = "/images/" + uploadedFile.FileName;
                        // сохраняем файл в папку Files в каталоге wwwroot
                        using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        restaurant.Image =uploadedFile.FileName;
                    }
                    unitOfWork.Restaurants.Update(restaurant);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }
               
                return View(restaurant);
            }
        }
        private void ValidatePosData(IUnitOfWork unitOfWork, string name)
        {
            var pos = unitOfWork.Food.GetByName(name);

            if (pos != null)
                ModelState.AddModelError("Error", $"Пункт с названием {name} уже существует");
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddComment( Comment comment)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (comment.UserId == 0)
                {
                    return NotFound();
                }
                if (comment.RestaurantId == 0)
                {
                    return NotFound();
                }

                var result = await _commentService.CreateCommentAsync(comment);
                return RedirectToAction("Index", "Restaurant", new { id = comment.RestaurantId });
            }
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restaurant = await _context.Restaurants
                .Include(p => p.Food)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restaurant == null)
            {
                return NotFound();
            }

            return View(restaurant);
        }

        // POST: Restaurant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restaurant = await _context.Restaurants.FindAsync(id);
            _context.Restaurants.Remove(restaurant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestaurantExists(int id)
        {
            return _context.Restaurants.Any(e => e.Id == id);
        }

    }
}