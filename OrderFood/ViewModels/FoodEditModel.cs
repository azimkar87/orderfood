﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace OrderFood.ViewModels
{
    public class FoodEditModel
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Наименование не должно быть пустым")]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "Ресторан")]
        public int RestaurantId { get; set; }

        public SelectList RestaurantsSelectList { get; set; }
    }
}