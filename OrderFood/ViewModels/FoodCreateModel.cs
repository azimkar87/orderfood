﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace OrderFood.ViewModels
{
    public class FoodCreateModel
    {

        [Required(ErrorMessage = "Наименование не должно быть пустым")]
        [Display(Name = "Наименование блюда")]
        public string Name { get; set; }

        [Display(Name = "Цена блюда")]
        public decimal Price { get;
            set; }

        [Required]
        [Display(Name = "Ресторан")]
        public int RestaurantId { get; set; }
        
        public SelectList RestaurantsSelectList { get; set; }
    }
}
