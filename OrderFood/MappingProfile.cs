﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.Domain.Dtos;
using OrderFood.ViewModels;

namespace OrderFood
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            FoodMapping();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateModel, Food>();
            CreateMap<Food, FoodEditModel>();
            CreateMap<FoodEditModel, Food>();
            CreateMap<FoodCreateModel, FoodCreateDto>();
            CreateMap<FoodCreateDto, Food>();
            CreateMap<OrderAddDto, Order>();
            CreateMap<BasketCreateDto, Basket>();
            CreateMap<Basket, BasketCreateDto>();
        }
    }
}
