﻿using System.ComponentModel.DataAnnotations;

namespace OrderFood.Domain.Dtos
{
    public class FoodCreateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int Price { get; set; }

        [Required]
        public int RestaurantId { get; set; }
    }
}
