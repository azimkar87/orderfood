﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OrderFood.Domain.Dtos
{
    public class OrderAddDto
    {
        [Required]
        public int FoodId { get; set; }

        [Required]
        public int RestaurantId { get; set; }

        [Required]

        public int Count { get; set; }

    }
}
