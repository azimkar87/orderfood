﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderFood.Domain.Dtos
{
    public class BasketCreateDto
    {
    [Required]
    public int UserId { get; set; }
    [Required]
    public DateTime OrderDate { get; set; }
    [Required]
    public int RestaurantId { get; set; }
    [Required]
    public int OrderSum { get; set; }
    }
}
