﻿using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Domain.Services
{
    public interface IRatingService
    {
        Task<EntityOperationResult<Rating>> CreateRatingAsync(Rating rating);
    }

    public class RatingService : IRatingService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RatingService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Rating>> CreateRatingAsync(Rating rating)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                { 
                    var entity = await unitOfWork.Rating.AddAsync(rating);
                    await unitOfWork.CompleteAsync();
                    return EntityOperationResult<Rating>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Rating>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
