﻿using OrderFood.Core.Models;
using OrderFood.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Domain.Services
{
    public interface IRestaurantService
    {
        Task<EntityOperationResult<Restaurant>> CreateFoodAsync(Restaurant restaurant);
    }

    public class RestaurantService : IRestaurantService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RestaurantService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Restaurant>> CreateFoodAsync(Restaurant restaurant)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var pos = unitOfWork.Restaurants.GetByName(restaurant.Name);

                if (pos != null)
                    return EntityOperationResult<Restaurant>
                        .Failure()
                        .AddError($"<Ресторан с именем {restaurant.Name} уже существует");

                try
                {

                    var entity = await unitOfWork.Restaurants.AddAsync(restaurant);
                    await unitOfWork.CompleteAsync();

                    return EntityOperationResult<Restaurant>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Restaurant>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
