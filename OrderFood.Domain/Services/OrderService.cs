﻿using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace OrderFood.Domain.Services
{
    public interface IOrderService
    {
        Task<EntityOperationResult<Order>> AddOrderAsync(Order order);
    }

    public class OrderService : IOrderService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Order>> AddOrderAsync(Order order)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                IEnumerable <Order> orders = unitOfWork.Order.GetAll().Where(x=>x.RestaurantId==order.RestaurantId);
                Order order2 = orders.Where(x=>x.FoodId==order.FoodId && x.UserId==order.UserId && x.isClosed==false).FirstOrDefault();

                if (order2 == null)
                { 
                try
                {
                         order.Count = 1;                  
                        var entity = await unitOfWork.Order.AddAsync(order);
                        await unitOfWork.CompleteAsync();
                        return EntityOperationResult<Order>.Success(entity);
                }
                    catch (Exception ex)
                    {
                        return EntityOperationResult<Order>.Failure().AddError(ex.Message);
                    }
                }
                else
                {
                try {
                    order.Count = order2.Count+1;
                    unitOfWork.Order.Update(order);
                    await unitOfWork.CompleteAsync();

                    return EntityOperationResult<Order>.Success(order);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Order>.Failure().AddError(ex.Message);
                }
            }
            }

        }

        }

}
