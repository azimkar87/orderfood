﻿using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Domain.Services
{
    public interface ICommentService
    {
        Task<EntityOperationResult<Comment>> CreateCommentAsync(Comment comment);
    }

    public class CommentService : ICommentService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CommentService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Comment>> CreateCommentAsync(Comment comment)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                try
                { 
                    var entity = await unitOfWork.Comment.AddAsync(comment);
                    await unitOfWork.CompleteAsync();
                    return EntityOperationResult<Comment>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Comment>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
