﻿using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Domain.Services
{
    public interface IBasketService
    {
        Task<EntityOperationResult<Basket>> CreateBasketAsync(BasketCreateDto createDto);
    }

    public class BasketService : IBasketService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public BasketService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Basket>> CreateBasketAsync(BasketCreateDto createDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
              
                try
                {

                    var basket = Mapper.Map<Basket>(createDto);
                   
                    var entity = await unitOfWork.Basket.AddAsync(basket);
                    
                    await unitOfWork.CompleteAsync();
                    var orderList = unitOfWork.Order.GetAll().Where(x => x.RestaurantId == basket.RestaurantId && x.UserId == basket.UserId && x.isClosed == false);
                    foreach (Order orderItem in orderList)
                    {
                        orderItem.BasketId = entity.Id;
                        orderItem.isClosed = true;
                        unitOfWork.Order.Update(orderItem);
                        await unitOfWork.CompleteAsync();
                    }
                    return EntityOperationResult<Basket>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Basket>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
