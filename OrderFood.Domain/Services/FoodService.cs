﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.DAL;
using FoodCreateDto = OrderFood.Domain.Dtos.FoodCreateDto;

namespace OrderFood.Domain.Services
{
    public interface IFoodService
    {
        Task<EntityOperationResult<Food>> CreateFoodAsync(FoodCreateDto createDto);
    }

    public class FoodService : IFoodService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public FoodService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if(unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Food>> CreateFoodAsync(FoodCreateDto createDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var pos = unitOfWork.Food.GetByName(createDto.Name);

                if (pos != null)
                    return EntityOperationResult<Food>
                        .Failure()
                        .AddError($"<Блюдо с именем {createDto.Name} уже существует");

                try
                {
                    var food = Mapper.Map<Food>(createDto);

                    var entity = await unitOfWork.Food.AddAsync(food);
                    await unitOfWork.CompleteAsync();

                    return EntityOperationResult<Food>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Food>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
