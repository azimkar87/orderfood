﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using OrderFood.Core.Models;
using OrderFood.Domain.Dtos;

namespace OrderFood.Domain
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            FoodMapping();
            OrderMapping();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateDto, Food>();
           
        }

        private void OrderMapping()
        {
            CreateMap<OrderAddDto, Order>();
        }
    }
}
