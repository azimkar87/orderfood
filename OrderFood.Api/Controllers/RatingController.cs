﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderFood.Core;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using OrderFood.Core.Models;

namespace OrderFood.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IRatingService _ratingService;

        public RatingController(IUnitOfWorkFactory unitOfWorkFactory, IRatingService ratingService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _ratingService = ratingService;
        }

        // GET api/
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var rating = unitOfWork.Rating.GetAll();

                return Ok(rating);
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Rating rating)
        {
            var result = await _ratingService.CreateRatingAsync(rating);

            if (result.IsSuccess)
            {
                return Ok(result.Entity);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
