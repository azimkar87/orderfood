﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderFood.Core;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;
using OrderFood.Core.Models;

namespace OrderFood.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IUnitOfWorkFactory unitOfWorkFactory, IRestaurantService restaurantService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _restaurantService = restaurantService;
        }

        // GET api/
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var restaurants = unitOfWork.Restaurants.GetAll();

                return Ok(restaurants);
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Restaurant restaurant)
        {
            var result = await _restaurantService.CreateFoodAsync(restaurant);

            if (result.IsSuccess)
            {
                return Ok(result.Entity);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
