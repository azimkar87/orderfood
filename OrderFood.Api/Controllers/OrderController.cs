﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderFood.Core.Models;
using OrderFood.DAL;
using OrderFood.Domain.Dtos;
using OrderFood.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace OrderFood.Api.Controllers
{
    public class OrderController : ControllerBase
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IOrderService _orderService;

        public OrderController(IUnitOfWorkFactory unitOfWorkFactory, IOrderService orderService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _orderService = orderService;
        }

        // GET api/
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var order = unitOfWork.Order.GetAll();

                return Ok(order);
            }
        }

        // GET api/values/5
        [HttpGet]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Order order)
        {
            var result = await _orderService.AddOrderAsync(order);

            if (result.IsSuccess)
            {
                return Ok(result.Entity);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}